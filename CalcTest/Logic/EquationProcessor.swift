//
//  EquationProcessor.swift
//  CalcTest
//
//  Created by Lukasz Koszentka on 25/01/2019.
//  Copyright © 2019 lukaszkoszentka. All rights reserved.
//

import Foundation

enum ProcessorError: Error {
    case unknown
    case wrongInput
}

protocol EquationProcessor {
    func calculate(eqationModelController: EquationModelControllable) throws -> ValueElement
}

class EquationProcessorImpl: EquationProcessor {
    
    private let valueStack: ValueStack
    private let operatorStack: OperatorStack
    private let basicCalculator: BasicEquationCalculator
    
    init(valueStack: ValueStack, operatorStack: OperatorStack, basicCalculator: BasicEquationCalculator) {
        self.operatorStack = operatorStack
        self.valueStack = valueStack
        self.basicCalculator = basicCalculator
    }
    
    func calculate(eqationModelController: EquationModelControllable) throws -> ValueElement {
        resetStacks()
        while !eqationModelController.isEquationEmpty() {
            guard let element = eqationModelController.getAndRemoveFirst() else { throw ProcessorError.unknown }
            try handleElement(element: element)
        }
        try clearOperatorStack()
        return try getIfScoreAvailable()
    }
    
    private func resetStacks() {
        valueStack.removeAll()
        operatorStack.removeAll()
    }
    
    private func clearOperatorStack() throws {
        while !operatorStack.isEmpty() {
            try process()
        }
    }
    
    private func getIfScoreAvailable() throws -> ValueElement {
        if !valueStack.isEmpty() && operatorStack.isEmpty(), let value = valueStack.stack.first {
            return value
        } else {
            throw ProcessorError.unknown
        }
    }
    
    private func handleElement(element: Element) throws {
        if let element = element as? ValueElement {
            valueStack.addElement(element: element)
        } else if let element = element as? OperatorElement {
            try handleOperator(element: element)
        } else {
            throw ProcessorError.wrongInput
        }
    }
    
    private func handleOperator(element: OperatorElement) throws {
        if operatorStack.isEmpty() || operatorStack.getLast() is LeftParenthesisElement {
            operatorStack.addElement(element: element)
        } else {
            try compareOperatorsAndProcess(element: element)
        }
    }
    
    private func compareOperatorsAndProcess(element: OperatorElement) throws {
        guard let lastElement = operatorStack.getLast() else { throw ProcessorError.unknown }
        if element is LeftParenthesisElement {
            operatorStack.addElement(element: element)
        } else if element is RightParenthesisElement {
            try handleOperatorAfterRithParenthesis()
        } else if element.isPriorityGreaterThan(element: lastElement) {
            operatorStack.addElement(element: element)
        } else {
            try process()
            try handleOperator(element: element)
        }
    }
    
    private func handleOperatorAfterRithParenthesis() throws {
        while true {
            guard let element = operatorStack.getLast() else { return }
            if element is LeftParenthesisElement {
                operatorStack.removeLastElements(ammount: 1)
                return
            } else {
                try process()
            }
        }
    }
    
    private func process() throws {
        guard let currentOperator = operatorStack.getAndRemoveLastElement(),
            let rightElement = valueStack.getAndRemoveLastElement(),
            let leftElement = valueStack.getAndRemoveLastElement() else { throw ProcessorError.unknown }
        
       try valueStack.addElement(element: basicCalculator.calculateSimpleEquation(currentOperator: currentOperator,
                                                leftElement: leftElement,
                                                rightElement: rightElement))
    }
    
}
