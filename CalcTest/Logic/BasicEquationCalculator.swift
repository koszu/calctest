//
//  BasicEquationCalculator.swift
//  CalcTest
//
//  Created by Lukasz Koszentka on 26/01/2019.
//  Copyright © 2019 lukaszkoszentka. All rights reserved.
//

import Foundation

protocol BasicEquationCalculator {
    func calculateSimpleEquation(currentOperator: OperatorElement, leftElement: ValueElement, rightElement: ValueElement) throws -> ValueElement
}

class BasicEquationCalculatorImpl: BasicEquationCalculator {
    
    func calculateSimpleEquation(currentOperator: OperatorElement, leftElement: ValueElement, rightElement: ValueElement) throws -> ValueElement {
        switch currentOperator.type {
        case .additionSign:
            return add(leftElement: leftElement, rightElement: rightElement)
        case .subtractionSign:
            return substract(leftElement: leftElement, rightElement: rightElement)
        case .divisionSign:
            return divide(leftElement: leftElement, rightElement: rightElement)
        case .multiplicationSign:
            return multiply(leftElement: leftElement, rightElement: rightElement)
        case .leftParenthesis:
            throw ProcessorError.unknown
        case .rightParenthesis:
            throw ProcessorError.unknown
        }
    }
    
    private func add(leftElement: ValueElement, rightElement: ValueElement) -> ValueElement {
        return NumberElement(value: leftElement.value + rightElement.value)
    }
    
    private func substract(leftElement: ValueElement, rightElement: ValueElement) -> ValueElement {
        return NumberElement(value: leftElement.value - rightElement.value)
    }
    
    private func multiply(leftElement: ValueElement, rightElement: ValueElement) -> ValueElement {
        return NumberElement(value: leftElement.value * rightElement.value)
    }
    
    private func divide(leftElement: ValueElement, rightElement: ValueElement) -> ValueElement {
        return NumberElement(value: leftElement.value / rightElement.value)
    }
    
}
