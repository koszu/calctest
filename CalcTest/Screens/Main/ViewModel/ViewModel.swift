//
//  MainViewModel.swift
//  CalcTest
//
//  Created by Lukasz Koszentka on 27/01/2019.
//  Copyright © 2019 lukaszkoszentka. All rights reserved.
//

import Foundation

enum ViewElementType {
    case additionSign
    case subtractionSign
    case multiplicationSign
    case divisionSign
    case leftParenthesis
    case rightParenthesis
}

protocol MainViewModelDelegeta: class {
    func updateValues()
}

protocol MainViewModel: class {
    var delegate: MainViewModelDelegeta? { get set }
    var equationText: String { get set }
    var currentNumberText: String { get set }
    var errorVisibe: Bool { get set }
    func addElementToCurrentNumber(element: String)
    func changeSignOfCurrentText()
    func addElementToEquation(elementType: ViewElementType)
    func calculate()
    func getEquationAsString() -> String
    func deleteLastElement()
}

class MainViewModelImpl: MainViewModel {
    
    weak var delegate: MainViewModelDelegeta?
    
    var equationText = ""
    var currentNumberText = ""
    var errorVisibe = false
    
    private let processor: EquationProcessor
    private let equationController: EquationModelController
    
    init(processor: EquationProcessor, equationController: EquationModelController) {
        self.processor = processor
        self.equationController = equationController
    }
    
    func getEquationAsString() -> String {
        return equationController.getStringFromEquation()
    }
    
    func calculate() {
        DispatchQueue.global(qos: .userInitiated).async {
            self.moveCurrentNumberToEquation()
            self.calculateEquation()
            self.delegate?.updateValues()
        }
    }
    
    func changeSignOfCurrentText() {
        if currentNumberText.first == "-" {
            currentNumberText.removeFirst()
        } else {
            currentNumberText.insert("-", at: currentNumberText.startIndex)
        }
        delegate?.updateValues()
    }
    
    func addElementToCurrentNumber(element: String) {
        currentNumberText = currentNumberText + element
        delegate?.updateValues()
    }
    
    func deleteLastElement() {
        equationController.deleteLastElement()
        delegate?.updateValues()
    }
    
    func addElementToEquation(elementType: ViewElementType) {
        moveCurrentNumberToEquation()
        switch elementType {
        case .additionSign:
            equationController.addElement(element: AdditionElement())
        case .subtractionSign:
            equationController.addElement(element: SubtractionElement())
        case .multiplicationSign:
            equationController.addElement(element: MultiplicationElement())
        case .divisionSign:
            equationController.addElement(element: DivisionElement())
        case .leftParenthesis:
            equationController.addElement(element: LeftParenthesisElement())
        case .rightParenthesis:
            equationController.addElement(element: RightParenthesisElement())
        }
        delegate?.updateValues()
    }
    
    private func calculateEquation() {
        do {
            equationController.addElement(element: NumberElement(value: try processor.calculate(eqationModelController: equationController).value))
            errorVisibe = false
        } catch {
            errorVisibe = true
        }
    }
    
    private func moveCurrentNumberToEquation() {
        if let value = Double(currentNumberText) {
            equationController.addElement(element: NumberElement(value: value))
            currentNumberText.removeAll()
        }
    }
    
}
