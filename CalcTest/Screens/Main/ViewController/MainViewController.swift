//
//  MainViewController.swift
//  CalcTest
//
//  Created by Lukasz Koszentka on 27/01/2019.
//  Copyright © 2019 lukaszkoszentka. All rights reserved.
//

import UIKit

class MainViewController: UIViewController {
    
    @IBOutlet weak var equationText: UILabel!
    @IBOutlet weak var currentText: UILabel!
    
    private let viewModel: MainViewModel
    
    init(nibName: String, viewModel: MainViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nibName, bundle: nil)
        self.viewModel.delegate = self
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        clearError()
    }
    
    @IBAction func numberAction(_ sender: Any) {
        guard let sender = sender as? UIButton else { return }
        viewModel.addElementToCurrentNumber(element: String(sender.tag))
    }
    
    @IBAction func leftParenthesisAction(_ sender: Any) {
        viewModel.addElementToEquation(elementType: .leftParenthesis)
    }
    
    @IBAction func rightParethesisAction(_ sender: Any) {
        viewModel.addElementToEquation(elementType: .rightParenthesis)
    }
    
    @IBAction func calculateAction(_ sender: Any) {
         viewModel.calculate()
    }
    
    @IBAction func substractAction(_ sender: Any) {
        viewModel.addElementToEquation(elementType: .subtractionSign)
    }
    
    @IBAction func addAction(_ sender: Any) {
        viewModel.addElementToEquation(elementType: .additionSign)
    }
    
    @IBAction func divideAction(_ sender: Any) {
        viewModel.addElementToEquation(elementType: .divisionSign)
    }
    
    @IBAction func multiplyAction(_ sender: Any) {
        viewModel.addElementToEquation(elementType: .multiplicationSign)
    }
    
    @IBAction func deleteAction(_ sender: Any) {
        viewModel.deleteLastElement()
    }
    
    @IBAction func dotAction(_ sender: Any) {
        viewModel.addElementToCurrentNumber(element: ".")
    }
    
    @IBAction func changeSignAction(_ sender: Any) {
        viewModel.changeSignOfCurrentText()
    }
    
    fileprivate func clearError() {
        setBorderToLabel(label: equationText, color: .gray)
        setBorderToLabel(label: currentText, color: .gray)
    }
    
    fileprivate func markFieldAsError() {
        setBorderToLabel(label: equationText, color: .red)
        setBorderToLabel(label: currentText, color: .red)
    }
    
    fileprivate func setBorderToLabel(label: UILabel, color: UIColor) {
        label.layer.borderColor = color.cgColor
        label.layer.borderWidth = 1
        label.layer.cornerRadius = 4
    }
    
}
extension MainViewController: MainViewModelDelegeta {
    
    func updateValues() {
        DispatchQueue.main.async {
            self.equationText.text = self.viewModel.getEquationAsString()
            self.currentText.text = self.viewModel.currentNumberText
            self.viewModel.errorVisibe ? self.markFieldAsError() : self.clearError()
        }
    }
    
}
