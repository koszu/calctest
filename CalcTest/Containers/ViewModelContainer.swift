//
//  ViewModelContainer.swift
//  CalcTest
//
//  Created by Lukasz Koszentka on 31/01/2019.
//  Copyright © 2019 lukaszkoszentka. All rights reserved.
//

import Foundation

class ViewModelContainer {
    
    private let workerContainer = WorkersContainer()
    
    func getMain() -> MainViewModel {
        return MainViewModelImpl(processor: workerContainer.getProcessor(),
                                 equationController: EquationModelController(equation: Equation(elements: [])))
    }
    
}
