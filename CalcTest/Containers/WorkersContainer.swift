//
//  WorkersContainer.swift
//  CalcTest
//
//  Created by Lukasz Koszentka on 04/02/2019.
//  Copyright © 2019 lukaszkoszentka. All rights reserved.
//

import Foundation

class WorkersContainer {
    
    func getProcessor() -> EquationProcessor {
        return EquationProcessorImpl(valueStack: getValueStack(), operatorStack: getOperatorStack(),
                                     basicCalculator: getBasicEquationCalculator())
    }
    
    func getValueStack() -> ValueStack {
        return ValueStack(stack: [])
    }
    
    func getOperatorStack() -> OperatorStack {
        return OperatorStack(stack: [])
    }
    
    func getBasicEquationCalculator() -> BasicEquationCalculator {
        return BasicEquationCalculatorImpl()
    }
    
}
