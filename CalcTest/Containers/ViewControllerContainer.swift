//
//  ViewControllerContainer.swift
//  CalcTest
//
//  Created by Lukasz Koszentka on 31/01/2019.
//  Copyright © 2019 lukaszkoszentka. All rights reserved.
//

import Foundation

class ViewControllerContainer {
    
    private let viewModelContainer = ViewModelContainer()
    
    func getMain() -> MainViewController {
        return MainViewController(nibName: String(describing: MainViewController.self), viewModel: viewModelContainer.getMain())
    }
    
}
