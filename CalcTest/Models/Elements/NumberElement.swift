//
//  NumberElement.swift
//  CalcTest
//
//  Created by Lukasz Koszentka on 25/01/2019.
//  Copyright © 2019 lukaszkoszentka. All rights reserved.
//

import Foundation

struct NumberElement: ValueElement {
    
    var value: Double
    
    init(value: Double) {
        self.value = value
    }
    
    func toString() -> String {
        return String(value)
    }
    
}
