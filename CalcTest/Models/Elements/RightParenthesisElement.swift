//
//  RightParenthesisElement.swift
//  CalcTest
//
//  Created by Lukasz Koszentka on 25/01/2019.
//  Copyright © 2019 lukaszkoszentka. All rights reserved.
//

import Foundation

struct RightParenthesisElement: OperatorElement {
    
    var type: ElementType
    var priority: ElementPriority
    
    init() {
        type = .rightParenthesis
        priority = .high
    }
    
    func toString() -> String {
        return ")"
    }
    
}
