//
//  DivisionElement.swift
//  CalcTest
//
//  Created by Lukasz Koszentka on 25/01/2019.
//  Copyright © 2019 lukaszkoszentka. All rights reserved.
//

import Foundation

struct DivisionElement: OperatorElement {
    
    var type: ElementType
    var priority: ElementPriority
    
    init() {
        type = .divisionSign
        priority = .medium
    }
    
    func toString() -> String {
        return "/"
    }
    
}
