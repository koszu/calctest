//
//  Element.swift
//  CalcTest
//
//  Created by Lukasz Koszentka on 25/01/2019.
//  Copyright © 2019 lukaszkoszentka. All rights reserved.
//

import Foundation

enum ElementType {
    case additionSign
    case subtractionSign
    case multiplicationSign
    case divisionSign
    case leftParenthesis
    case rightParenthesis
}

enum ElementPriority: Int {
    case low // add and subtraction
    case medium // multiply and divide
    case high // Parenthesis
}

protocol Element {
    func toString() -> String
}

protocol OperatorElement: Element {
    var type: ElementType { get }
    var priority: ElementPriority { get }
    func isPriorityGreaterThan(element: OperatorElement) -> Bool
}

extension OperatorElement {
    
    func isPriorityGreaterThan(element: OperatorElement) -> Bool {
        return priority.rawValue > element.priority.rawValue
    }
    
}

protocol ValueElement: Element {
    var value: Double { get }
}
