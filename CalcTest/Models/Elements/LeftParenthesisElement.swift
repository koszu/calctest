//
//  LeftParenthesisElement.swift
//  CalcTest
//
//  Created by Lukasz Koszentka on 25/01/2019.
//  Copyright © 2019 lukaszkoszentka. All rights reserved.
//

import Foundation

struct LeftParenthesisElement: OperatorElement {
    
    var type: ElementType
    var priority: ElementPriority
    
    init() {
        type = .leftParenthesis
        priority = .high
    }
    
    func toString() -> String {
        return "("
    }
    
}
