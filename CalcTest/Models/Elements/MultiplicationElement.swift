//
//  MultiplicationElement.swift
//  CalcTest
//
//  Created by Lukasz Koszentka on 25/01/2019.
//  Copyright © 2019 lukaszkoszentka. All rights reserved.
//

import Foundation

struct MultiplicationElement: OperatorElement {
    
    var type: ElementType
    var priority: ElementPriority
    
    init() {
        type = .multiplicationSign
        priority = .medium
    }
    
    func toString() -> String {
        return "*"
    }
    
}
