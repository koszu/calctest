//
//  SubtractionElement.swift
//  CalcTest
//
//  Created by Lukasz Koszentka on 25/01/2019.
//  Copyright © 2019 lukaszkoszentka. All rights reserved.
//

import Foundation

struct SubtractionElement: OperatorElement {
    
    var type: ElementType
    var priority: ElementPriority
    
    init() {
        type = .subtractionSign
        priority = .low
    }
    
    func toString() -> String {
        return "-"
    }
    
}
