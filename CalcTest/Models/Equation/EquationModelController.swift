//
//  EqationModelController.swift
//  CalcTest
//
//  Created by Lukasz Koszentka on 25/01/2019.
//  Copyright © 2019 lukaszkoszentka. All rights reserved.
//

import Foundation

protocol EquationModelControllable {
    func addElement(element: Element)
    func addElements(elements: [Element]) 
    func deleteLastElement()
    func getStringFromEquation() -> String
    func getAndRemoveFirst() -> Element?
    func isEquationEmpty() -> Bool 
}

class EquationModelController: EquationModelControllable {
    
    private let equation: Equation
    
    init(equation: Equation) {
        self.equation = equation
    }
    
    func isEquationEmpty() -> Bool {
        return equation.elements.isEmpty
    }
    
    func getAndRemoveFirst() -> Element? {
        return equation.elements.removeFirst()
    }
    
    func addElements(elements: [Element]) {
        equation.elements.append(contentsOf: elements)
    }
    
    func addElement(element: Element) {
        equation.elements.append(element)
    }
    
    func deleteLastElement() {
        if !equation.elements.isEmpty {
            equation.elements.removeLast()
        }
    }
    
    func getStringFromEquation() -> String {
        return equation.elements.map({ $0.toString() }).joined(separator: " ")
    }
    
}


