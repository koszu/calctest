//
//  Equation.swift
//  CalcTest
//
//  Created by Lukasz Koszentka on 25/01/2019.
//  Copyright © 2019 lukaszkoszentka. All rights reserved.
//

import Foundation

class Equation {
    
    var elements: [Element]
    
    init(elements: [Element]) {
        self.elements = elements
    }
    
}
