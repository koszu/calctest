//
//  ValueStack.swift
//  CalcTest
//
//  Created by Lukasz Koszentka on 25/01/2019.
//  Copyright © 2019 lukaszkoszentka. All rights reserved.
//

import Foundation

class ValueStack: Stack {
    
    var stack: [ValueElement]
    
    init(stack: [ValueElement]) {
        self.stack = stack
    }
    
    func removeAll() {
        stack.removeAll()
    }
    
    func addElement(element: ValueElement) {
        stack.append(element)
    }
    
    func removeLastElements(ammount: Int) {
        stack.removeLast(ammount)
    }
    
    func isEmpty() -> Bool {
        return stack.isEmpty
    }
    
    func getLast() -> ValueElement? {
        return stack.last
    }
    
    func getAndRemoveLastElement() -> ValueElement? {
        return !stack.isEmpty ? stack.removeLast() : nil
    }
    
}
