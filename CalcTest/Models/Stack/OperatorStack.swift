//
//  OperatorStack.swift
//  CalcTest
//
//  Created by Lukasz Koszentka on 25/01/2019.
//  Copyright © 2019 lukaszkoszentka. All rights reserved.
//

import Foundation

class OperatorStack: Stack {
    
    private var stack: [OperatorElement]
    
    init(stack: [OperatorElement]) {
        self.stack = stack
    }
    
    func removeAll() {
        stack.removeAll()
    }
    
    func addElement(element: OperatorElement) {
        stack.append(element)
    }
    
    func removeLastElements(ammount: Int) {
        stack.removeLast(ammount)
    }
    
    func isEmpty() -> Bool {
        return stack.isEmpty
    }
    
    func getLast() -> OperatorElement? {
        return stack.last
    }
    
    func getAndRemoveLastElement() -> OperatorElement? {
        return stack.removeLast()
    }
    
}
