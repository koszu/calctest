//
//  Stack.swift
//  CalcTest
//
//  Created by Lukasz Koszentka on 25/01/2019.
//  Copyright © 2019 lukaszkoszentka. All rights reserved.
//

import Foundation

protocol Stack {
    associatedtype ElementStackType
    func addElement(element: ElementStackType)
    func removeLastElements(ammount: Int)
    func isEmpty() -> Bool
    func getLast() -> ElementStackType?
    func getAndRemoveLastElement() -> ElementStackType?
    func removeAll()
}
