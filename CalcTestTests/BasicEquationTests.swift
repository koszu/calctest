//
//  BasicEquationTests.swift
//  CalcTestTests
//
//  Created by Lukasz Koszentka on 26/01/2019.
//  Copyright © 2019 lukaszkoszentka. All rights reserved.
//

@testable import CalcTest

import XCTest
import Foundation

class BasicEquationTests: XCTestCase {
    
    var elements: [Element]!
    var equation: Equation!
    var controller: EquationModelController!
    var processor: EquationProcessor!
    var accuracy = 0.000001
    
    override func setUp() {
        elements = []
        equation = Equation(elements: [])
        controller = EquationModelController(equation: equation)
        processor = EquationProcessorImpl(valueStack: ValueStack(stack: []), operatorStack: OperatorStack(stack: []), basicCalculator: BasicEquationCalculatorImpl())
    }
    
    func testEquations() {
        checkEquation(characters: Array("1*6+5*6"), result: 36.0)
        checkEquation(characters: Array("1.5/3+2.5*3"), result: 8.0)
        checkEquation(characters: Array("1.5*3*15"), result: 67.5)
        checkEquation(characters: Array("1.5*3*-15"), result: -67.5)
        checkEquation(characters: Array("-123--876*2*45-123.123"), result: 78593.877)
        checkEquation(characters: Array("11-123.123/4-76.12*98.23-12"), result: -7509.04835)
        checkEquation(characters: Array("-123*-876*2/45/123.123"), result: 38.894439)
        checkEquation(characters: Array("(1+6)/-7)"), result: -1.0)
        checkEquation(characters: Array("-7/(1+6)"), result: -1.0)
        checkEquation(characters: Array("-7/(1+6)*(4+-7)"), result: 3.0)
        checkEquation(characters: Array("((5+1115)/-40)"), result: -28.0)
        checkEquation(characters: Array("44-(7+5)"), result: 32.0)
        checkEquation(characters: Array("6*8/4"), result: 12.0)
        checkEquation(characters: Array("81/9-(32-27)"), result: 4.0)
        checkEquation(characters: Array("25-(5*2+2*3+9)"), result: 0)
        checkEquation(characters: Array("(14500+1234.12)*1.23"), result: 19352.9676)
        checkEquation(characters: Array("(10500+94.93)*1.23"), result: 13031.7639)
        checkEquation(characters: Array("120000-(120000-(120000/1.23))/2"), result: 108780.487804)
        checkEquation(characters: Array("120000-(120000-(120000-(120000/1.23))/2)"), result: 11219.512195)
        checkEquation(characters: Array("(120000-(120000-(120000/1.23))/2)*0.75*0.18"), result: 14685.365853)
        checkEquation(characters: Array("120000-(14685.365853+11219.512195)"), result: 94095.121952)
        checkEquation(characters: Array("94095.121952/120000"), result: 0.784126)
        checkEquation(characters: Array("94095.121952/60"), result: 1568.252032)
    }
    
    private func checkEquation(characters: [Character], result: Double) {
        setUp()
        controller.addElements(elements: parseEquationFrom(characters: characters))
        XCTAssertEqual(try processor.calculate(eqationModelController: controller).value, result, accuracy: accuracy)
    }
    
}
