//
//  XCTestCase+ParseStringEquation.swift
//  CalcTestTests
//
//  Created by Lukasz Koszentka on 27/01/2019.
//  Copyright © 2019 lukaszkoszentka. All rights reserved.
//

@testable import CalcTest

import XCTest
import Foundation

extension XCTestCase {
    
    func parseEquationFrom(characters: [Character]) -> [Element] {
        var elements: [Element] = []
        var newCharacters = characters
        var currentElement = ""
        while !newCharacters.isEmpty {
            let element = String(newCharacters.removeFirst())
            switch element {
            case "+":
                addNumberElementIfValid(etringElement: currentElement, elemetns: &elements)
                elements.append(AdditionElement())
                currentElement.removeAll()
            case "-":
                if elements.isEmpty && currentElement.isEmpty {
                    currentElement = currentElement + String(element)
                } else if elements.isEmpty && !currentElement.isEmpty {
                    addNumberElementIfValid(etringElement: currentElement, elemetns: &elements)
                    elements.append(SubtractionElement())
                    currentElement.removeAll()
                } else if let last = elements.last, last is NumberElement {
                    addNumberElementIfValid(etringElement: currentElement, elemetns: &elements)
                    elements.append(SubtractionElement())
                    currentElement.removeAll()
                } else if let last = elements.last, last is OperatorElement && currentElement.isEmpty {
                    currentElement = currentElement + String(element)
                } else if let last = elements.last, last is OperatorElement && !currentElement.isEmpty {
                    addNumberElementIfValid(etringElement: currentElement, elemetns: &elements)
                    elements.append(SubtractionElement())
                    currentElement.removeAll()
                }
            case "/":
                addNumberElementIfValid(etringElement: currentElement, elemetns: &elements)
                elements.append(DivisionElement())
                currentElement.removeAll()
            case "*":
                addNumberElementIfValid(etringElement: currentElement, elemetns: &elements)
                elements.append(MultiplicationElement())
                currentElement.removeAll()
            case "(":
                addNumberElementIfValid(etringElement: currentElement, elemetns: &elements)
                elements.append(LeftParenthesisElement())
                currentElement.removeAll()
            case ")":
                addNumberElementIfValid(etringElement: currentElement, elemetns: &elements)
                elements.append(RightParenthesisElement())
                currentElement.removeAll()
            default:
                currentElement = currentElement + String(element)
                if newCharacters.isEmpty {
                    addNumberElementIfValid(etringElement: currentElement, elemetns: &elements)
                }
            }
        }
        return elements
    }
    
    private func addNumberElementIfValid(etringElement: String, elemetns: inout [Element]) {
        if let value = Double(etringElement) {
            elemetns.append(NumberElement(value: value))
        }
    }
    
}
